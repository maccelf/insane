# frozen_string_literal: true

require_relative 'config/helper'
require 'irb/completion'

IRB.conf[:AUTO_INDENT] = true
IRB.conf[:SAVE_HISTORY] = 1000
