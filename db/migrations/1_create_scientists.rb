# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:scientists) do
      primary_key :id
      String :name, unique: true, null: false
      Float :insane, null: false
      Integer :destroy_galaxy, null: false
      DateTime :datetime, null: false
    end
  end
end
