# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:weapons) do
      primary_key :id
      String :name, unique: true, null: false
      Float :power, null: false
      foreign_key :scientist_id, :scientists,
                  on_delete: :cascade,
                  on_update: :cascade
      DateTime :datetime, null: false
    end
  end
end
