# frozen_string_literal: true

RSpec.describe Insane::Models::Weapon, :weapon do
  subject { described_class }

  it { is_expected.to respond_to(:new, :create, :count) }

  describe '.new' do
    subject(:instance) { described_class.new(params) }

    let(:method) { subject }
    let(:params) { {} }

    it 'creates an instance of the model' do
      expect(instance).to be_instance_of(described_class)
    end

    it 'does not create a record in database' do
      expect { method }.not_to change(described_class, :count)
    end

    context 'when the `id` is passed along with the parameters' do
      let(:params) { { id: 5 } }

      specify do
        expect { method }.to raise_error(Sequel::MassAssignmentRestriction)
      end
    end

    describe 'created instance' do
      subject { instance }

      context 'when all the required parameters are passed and correct' do
        let(:params) { attributes_for(:weapon) }

        it { is_expected.to be_valid }
      end

      context 'when the name parameter is missing' do
        let(:params) { attributes_for(:weapon).except(:name) }

        it { is_expected.not_to be_valid }
      end

      context 'when the name parameter is not converted to String' do
        let(:params) { attributes_for(:weapon, name: nil) }

        it { is_expected.not_to be_valid }
      end

      context 'when name is already belongs to another weapon' do
        before { create(:weapon, name: 'What-if machine') }

        let(:params) { attributes_for(:weapon, name: 'What-if machine') }

        it { is_expected.not_to be_valid }
      end

      context 'when the power parameter is missing' do
        let(:params) { attributes_for(:weapon).except(:power) }

        it { is_expected.not_to be_valid }
      end

      context 'when the power parameter is not converted to Numeric' do
        let(:params) { attributes_for(:weapon, power: 'strong') }

        it { is_expected.not_to be_valid }
      end

      context 'when the power parameter is negative' do
        let(:params) { attributes_for(:weapon, power: -36.6) }

        it { is_expected.not_to be_valid }
      end

      context 'when the scientist_id parameter is missing' do
        let(:params) { attributes_for(:weapon).except(:scientist_id) }

        it { is_expected.not_to be_valid }
      end

      context 'when scientist_id parameter points to a nonexistent scientist' do
        let(:params) { attributes_for(:weapon, scientist_id: 0) }

        it { is_expected.not_to be_valid }
      end
    end
  end

  describe '.count' do
    subject(:count) { described_class.count }

    before { create_list(:weapon, weapon_count) }

    let(:weapon_count) { 3 }

    it 'returns count of records in database' do
      expect(count).to eql weapon_count
    end
  end

  describe '.create' do
    subject(:instance) { described_class.create(params) }

    let(:method) { subject }
    let(:params) { attributes_for(:weapon) }

    context 'when all the required parameters are passed and correct' do
      it 'creates an instance of the model' do
        expect(instance).to be_instance_of(described_class)
      end

      it 'creates a record in database' do
        expect { method }.to change(described_class, :count)
      end
    end

    context 'when the `id` is passed along with the parameters' do
      let(:params) { { id: 5 } }

      specify do
        expect { method }.to raise_error(Sequel::MassAssignmentRestriction)
      end
    end

    context 'when the name parameter is missing' do
      let(:params) { attributes_for(:weapon).except(:name) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the name parameter is not converted to String' do
      let(:params) { attributes_for(:weapon, name: nil) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when name is already belongs to another weapon' do
      before { create(:weapon, name: 'What-if machine') }

      let(:params) { attributes_for(:weapon, name: 'What-if machine') }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the power parameter is missing' do
      let(:params) { attributes_for(:weapon).except(:power) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the power parameter is not converted to Numeric' do
      let(:params) { attributes_for(:weapon, power: 'strong') }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the power parameter is negative' do
      let(:params) { attributes_for(:weapon, power: -36.6) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the scientist_id parameter is missing' do
      let(:params) { attributes_for(:weapon).except(:scientist_id) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when scientist_id parameter points to a nonexistent scientist' do
      let(:params) { attributes_for(:weapon, scientist_id: 0) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end
  end

  describe 'instance of the model' do
    subject(:instance) { create(:weapon) }

    let(:methods) do
      %i[id datetime name power scientist_id scientist update delete]
    end

    it { is_expected.to respond_to(*methods) }
  end

  describe '#id' do
    subject(:id) { instance.id }

    context 'when weapon was instantiated by `.new`' do
      let(:instance) { build(:weapon) }

      it { is_expected.to be_nil }
    end

    context 'when weapon was instantiated by `.create`' do
      let(:instance) { create(:weapon) }

      it { is_expected.to be_a(Integer) }
    end
  end

  describe '#datetime' do
    subject(:datetime) { instance.datetime }

    let(:time) { '2020-01-01 00:00:00' }

    context 'when weapon was instantiated by `.new`' do
      let(:instance) { build(:weapon) }

      it { is_expected.to be_nil }

      context 'when the `datetime` is passed along with the parameters' do
        let(:instance) { build(:weapon, datetime: time) }

        it 'matches the parameter `datetime`' do
          expect(datetime.strftime('%F %T')).to eql time
        end
      end
    end

    context 'when weapon was instantiated be `.create`' do
      let(:instance) { create(:weapon) }

      it { is_expected.to be_a(Time) }

      context 'when the `datetime` is passed along with the parameters' do
        let(:instance) { create(:weapon, datetime: time) }
        let(:now) { Time.now }

        it 'does not match the parameter `datetime`' do
          expect(datetime.strftime('%F %T')).not_to eql time
        end

        it 'matches the current time' do
          expect(datetime.to_s).to eql now.to_s
        end
      end
    end
  end

  describe '#name' do
    subject(:name) { instance.name }

    let(:instance) { create(:weapon) }

    it { is_expected.to be_a(String) }
  end

  describe '#power' do
    subject(:power) { instance.power }

    let(:instance) { create(:weapon) }

    it { is_expected.to be_a(Numeric) }
  end

  describe '#scientist_id' do
    subject(:scientist_id) { instance.scientist_id }

    let(:instance) { create(:weapon) }

    it { is_expected.to be_a(Integer) }
  end

  describe '#scientist' do
    subject(:scientist) { instance.scientist }

    let(:instance) { create(:weapon) }

    it { is_expected.to be_instance_of(Insane::Models::Scientist) }
  end

  describe '#update' do
    subject(:method) { instance.update(params) }

    let(:instance) { create(:weapon) }

    context 'when `id` is specified' do
      let(:params) { { id: 'new id' } }

      specify do
        expect { method }.to raise_error(Sequel::MassAssignmentRestriction)
      end
    end

    context 'when `name` is specified' do
      let(:params) { { name: name } }

      context 'when `name` is not converted to String' do
        let(:name) { nil }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end

      context 'when `name` is correct' do
        let(:name) { 'New name' }

        it { expect { method }.to change(instance, :name).to(name) }
      end

      context 'when `name` is already belongs to another scientist' do
        before { create(:weapon, name: name) }

        let(:name) { 'What-if machine' }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end
    end

    context 'when `power` is specified' do
      let(:params) { { power: power } }

      context 'when `power` is not converted to Numeric' do
        let(:power) { 'Strong' }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end

      context 'when `power` is correct' do
        let(:power) { 36.6 }

        it { expect { method }.to change(instance, :power).to(power) }
      end

      context 'when `power` is negative' do
        let(:power) { -36.6 }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end
    end

    context 'when `scientist_id` is specified' do
      let(:params) { { scientist_id: scientist_id } }

      context 'when `scientist_id` is correct' do
        let(:scientist_id) { create(:scientist).id }

        specify do
          expect { method }.to change(instance, :scientist_id).to(scientist_id)
        end
      end

      context 'when `scientist_id` is points to a nonexistent scientist' do
        let(:scientist_id) { 0 }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end
    end

    context 'when the `datetime` is specified' do
      let(:params) { { datetime: datetime } }
      let(:datetime) { DateTime.parse('2020-01-01') }

      specify do
        expect { method }.to change(instance, :datetime).to(datetime.to_time)
      end
    end
  end

  describe '#delete' do
    subject(:method) { instance.delete }

    let!(:instance) { create(:weapon) }

    it 'deletes the record' do
      expect { method }.to change(described_class, :count).by(-1)
    end
  end
end
