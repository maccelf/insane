# frozen_string_literal: true

RSpec.describe Insane::Models::Scientist, :scientist do
  subject { described_class }

  it { is_expected.to respond_to(:new, :create, :count) }

  describe '.new' do
    subject(:instance) { described_class.new(params) }

    let(:method) { subject }
    let(:params) { {} }

    it 'creates an instance of the model' do
      expect(instance).to be_instance_of(described_class)
    end

    it 'does not create a record in database' do
      expect { method }.not_to change(described_class, :count)
    end

    context 'when the `id` is passed along with the parameters' do
      let(:params) { { id: 5 } }

      specify do
        expect { method }.to raise_error(Sequel::MassAssignmentRestriction)
      end
    end

    describe 'created instance' do
      subject { instance }

      context 'when all the required parameters are passed and correct' do
        let(:params) { attributes_for(:scientist) }

        it { is_expected.to be_valid }
      end

      context 'when the name parameter is missing' do
        let(:params) { attributes_for(:scientist).except(:name) }

        it { is_expected.not_to be_valid }
      end

      context 'when the name parameter is not converted to String' do
        let(:params) { attributes_for(:scientist, name: nil) }

        it { is_expected.not_to be_valid }
      end

      context 'when name is already belongs to another scientist' do
        before { create(:scientist, name: 'Orochimaru') }

        let(:params) { attributes_for(:scientist, name: 'Orochimaru') }

        it { is_expected.not_to be_valid }
      end

      context 'when the insane parameter is missing' do
        let(:params) { attributes_for(:scientist).except(:insane) }

        it { is_expected.not_to be_valid }
      end

      context 'when the insane parameter is not converted to Numeric' do
        let(:params) { attributes_for(:scientist, insane: 'crazy') }

        it { is_expected.not_to be_valid }
      end

      context 'when the insane parameter is negative' do
        let(:params) { attributes_for(:scientist, insane: -36.6) }

        it { is_expected.not_to be_valid }
      end

      context 'when the destroy_galaxy parameter is missing' do
        let(:params) { attributes_for(:scientist).except(:destroy_galaxy) }

        it { is_expected.not_to be_valid }
      end

      context 'when the destroy_galaxy parameter is not converted to Integer' do
        let(:params) { attributes_for(:scientist, destroy_galaxy: '36.6') }

        it { is_expected.not_to be_valid }
      end

      context 'when the destroy_galaxy parameter is negative' do
        let(:params) { attributes_for(:scientist, destroy_galaxy: -15) }

        it { is_expected.not_to be_valid }
      end
    end
  end

  describe '.count' do
    subject(:count) { described_class.count }

    before { create_list(:scientist, scientist_count) }

    let(:scientist_count) { 3 }

    it 'returns count of records in database' do
      expect(count).to eql scientist_count
    end
  end

  describe '.create' do
    subject(:instance) { described_class.create(params) }

    let(:method) { subject }
    let(:params) { attributes_for(:scientist) }

    context 'when all the required parameters are passed and correct' do
      it 'creates an instance of the model' do
        expect(instance).to be_instance_of(described_class)
      end

      it 'creates a record in database' do
        expect { method }.to change(described_class, :count)
      end
    end

    context 'when the `id` is passed along with the parameters' do
      let(:params) { { id: 5 } }

      specify do
        expect { method }.to raise_error(Sequel::MassAssignmentRestriction)
      end
    end

    context 'when the name parameter is missing' do
      let(:params) { attributes_for(:scientist).except(:name) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the name parameter is not converted to String' do
      let(:params) { attributes_for(:scientist, name: nil) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when name is already belongs to another scientist' do
      before { create(:scientist, name: 'Orochimaru') }

      let(:params) { attributes_for(:scientist, name: 'Orochimaru') }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the insane parameter is missing' do
      let(:params) { attributes_for(:scientist).except(:insane) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the insane parameter is not converted to Numeric' do
      let(:params) { attributes_for(:scientist, insane: 'crazy') }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the insane parameter is negative' do
      let(:params) { attributes_for(:scientist, insane: -36.6) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the destroy_galaxy parameter is missing' do
      let(:params) { attributes_for(:scientist).except(:destroy_galaxy) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the destroy_galaxy parameter is not converted to Integer' do
      let(:params) { attributes_for(:scientist, destroy_galaxy: '36.6') }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end

    context 'when the destroy_galaxy parameter is negative' do
      let(:params) { attributes_for(:scientist, destroy_galaxy: -15) }

      specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
    end
  end

  describe 'instance of the model' do
    subject(:instance) { create(:scientist) }

    let(:methods) do
      %i[id datetime name insane destroy_galaxy weapons update delete]
    end

    it { is_expected.to respond_to(*methods) }
  end

  describe '#id' do
    subject(:id) { instance.id }

    context 'when scientist was instantiated by `.new`' do
      let(:instance) { build(:scientist) }

      it { is_expected.to be_nil }
    end

    context 'when scientist was instantiated by `.create`' do
      let(:instance) { create(:scientist) }

      it { is_expected.to be_a(Integer) }
    end
  end

  describe '#datetime' do
    subject(:datetime) { instance.datetime }

    let(:time) { '2020-01-01 00:00:00' }

    context 'when scientist was instantiated by `.new`' do
      let(:instance) { build(:scientist) }

      it { is_expected.to be_nil }

      context 'when the `datetime` is passed along with the parameters' do
        let(:instance) { build(:scientist, datetime: time) }

        it 'matches the parameter `datetime`' do
          expect(datetime.strftime('%F %T')).to eql time
        end
      end
    end

    context 'when scientist was instantiated by `.create`' do
      let(:instance) { create(:scientist) }

      it { is_expected.to be_a(Time) }

      context 'when the `datetime` is passed along with the parameters' do
        let(:instance) { create(:scientist, datetime: time) }
        let(:now) { Time.now }

        it 'does not match the parameter `datetime`' do
          expect(datetime.strftime('%F %T')).not_to eql time
        end

        it 'matches the current time' do
          expect(datetime.to_s).to eql now.to_s
        end
      end
    end
  end

  describe '#name' do
    subject(:name) { instance.name }

    let(:instance) { create(:scientist) }

    it { is_expected.to be_a(String) }
  end

  describe '#insane' do
    subject(:insane) { instance.insane }

    let(:instance) { create(:scientist) }

    it { is_expected.to be_a(Numeric) }
  end

  describe '#destroy_galaxy' do
    subject(:destroy_galaxy) { instance.destroy_galaxy }

    let(:instance) { create(:scientist) }

    it { is_expected.to be_a(Integer) }
  end

  describe '#weapons' do
    subject(:weapons) { instance.weapons }

    let(:instance) { create(:scientist) }

    it { is_expected.to be_an(Array) }

    context 'when scientist does not have dependent weapon' do
      it { is_expected.to be_empty }
    end

    context 'when scientist has dependent weapon' do
      before { create_list(:weapon, weapon_count, scientist_id: instance.id) }

      let(:weapon_count) { 3 }

      it { is_expected.to all(be_instance_of(Insane::Models::Weapon)) }

      it 'returns all weapon records' do
        expect(weapons.count).to eql weapon_count
      end
    end
  end

  describe '#update' do
    subject(:method) { instance.update(params) }

    let(:instance) { create(:scientist) }

    context 'when `id` is specified' do
      let(:params) { { id: 'new id' } }

      specify do
        expect { method }.to raise_error(Sequel::MassAssignmentRestriction)
      end
    end

    context 'when `name` is specified' do
      let(:params) { { name: name } }

      context 'when `name` is not converted to String' do
        let(:name) { nil }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end

      context 'when `name` is correct' do
        let(:name) { 'New name' }

        it { expect { method }.to change(instance, :name).to(name) }
      end

      context 'when `name` is already belongs to another scientist' do
        before { create(:scientist, name: name) }

        let(:name) { 'Orochimaru' }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end
    end

    context 'when `insane` is specified' do
      let(:params) { { insane: insane } }

      context 'when `insane` is not converted to Numeric' do
        let(:insane) { 'Crazy' }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end

      context 'when `insane` is correct' do
        let(:insane) { 36.6 }

        it { expect { method }.to change(instance, :insane).to(insane) }
      end

      context 'when `insane` is negative' do
        let(:insane) { -36.6 }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end
    end

    context 'when `destroy_galaxy` is specified' do
      let(:params) { { destroy_galaxy: destroy } }

      context 'when `destroy_galaxy` is not converted to Integer' do
        let(:destroy) { '36.6' }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end

      context 'when `destroy_galaxy` is correct' do
        let(:destroy) { 27 }

        specify do
          expect { method }.to change(instance, :destroy_galaxy).to(destroy)
        end
      end

      context 'when `destroy_galaxy` is negative' do
        let(:destroy) { -15 }

        specify { expect { method }.to raise_error(Sequel::ValidationFailed) }
      end
    end

    context 'when the `datetime` is specified' do
      let(:params) { { datetime: datetime } }
      let(:datetime) { DateTime.parse('2020-01-01') }

      specify do
        expect { method }.to change(instance, :datetime).to(datetime.to_time)
      end
    end
  end

  describe '#delete' do
    subject(:method) { instance.delete }

    let!(:instance) { create(:scientist) }

    it 'deletes the record' do
      expect { method }.to change(described_class, :count).by(-1)
    end

    context 'when scientist has a dependent weapon' do
      before { create_list(:weapon, count, scientist_id: scientist_id) }

      let(:count) { 3 }
      let(:scientist_id) { instance.id }

      it 'deletes records of weapons' do
        expect { method }.to change(Insane::Models::Weapon, :count).by(-count)
      end
    end
  end
end
