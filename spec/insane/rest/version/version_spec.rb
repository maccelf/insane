# frozen_string_literal: true

RSpec.describe Insane::REST::Version, :version do
  describe 'GET /version' do
    subject(:response) { get '/version' }

    it { is_expected.to be_ok }

    it 'returns version of the application' do
      expect(response.body).to eql Insane::VERSION
    end
  end
end
