# frozen_string_literal: true

RSpec.describe Insane::REST::Weapon, :index do
  describe 'GET /scientists/:id/weapons' do
    subject(:response) { get "/scientists/#{id}/weapons" }

    let(:content) { JSON.parse(response.body) }
    let(:scientist) { create(:scientist) }
    let(:id) { scientist.id }

    it { is_expected.to be_ok }

    it 'returns array' do
      expect(content).to be_an(Array)
    end

    context 'when there is no weapon in the database' do
      it 'returns empty array' do
        expect(content).to be_empty
      end
    end

    context 'when weapons are in the database' do
      before { create_list(:weapon, scientist_count, scientist_id: id) }

      let(:scientist_count) { 3 }

      it 'returns an array with all scientist weapons' do
        expect(content.count).to eql scientist_count
      end
    end

    context 'when scientist is does not exist' do
      let(:id) { scientist.delete.id }

      it { is_expected.to be_not_found }
    end
  end
end
