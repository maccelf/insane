# frozen_string_literal: true

RSpec.describe Insane::REST::Weapon, :create do
  describe 'POST /scientists/:id/weapons' do
    subject(:response) { post "/scientists/#{id}/weapons", params.to_json }

    let(:id) { create(:scientist).id }

    context 'when data is valid' do
      let(:params) { attributes_for(:weapon).except(:scientist_id) }

      it { is_expected.to be_created }

      it 'returns result of adding' do
        expect(response.body).not_to be_empty
      end

      describe 'result of adding' do
        subject(:result) { response.body }

        it 'matches the passed parameters' do
          expect(JSON.parse(result, symbolize_names: true)).to include(params)
        end
      end
    end

    context 'when data is not valid' do
      let(:params) { attributes_for(:weapon).except(:name, :scientist_id) }

      it { is_expected.to be_bad_request }

      it 'returns errors' do
        expect(response.body).not_to be_empty
      end
    end
  end
end
