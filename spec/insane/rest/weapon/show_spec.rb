# frozen_string_literal: true

RSpec.describe Insane::REST::Weapon, :show do
  describe 'GET /weapons/:id' do
    subject(:response) { get "/weapons/#{id}" }

    context 'when weapon exists' do
      let(:weapon) { create(:weapon) }
      let(:id) { weapon.id }

      it { is_expected.to be_ok }

      it 'returns weapon data' do
        expect(response.body).to eql weapon.values.to_json
      end
    end

    context 'when weapon does not exist' do
      let(:weapon) { create(:weapon) }
      let(:id) { weapon.delete.id }

      it { is_expected.to be_not_found }
    end
  end
end
