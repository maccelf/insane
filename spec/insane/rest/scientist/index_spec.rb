# frozen_string_literal: true

RSpec.describe Insane::REST::Scientist, :index do
  describe 'GET /scientists' do
    subject(:response) { get '/scientists' }

    let(:content) { JSON.parse(response.body) }

    it { is_expected.to be_ok }

    it 'returns array' do
      expect(content).to be_an(Array)
    end

    context 'when there is no scientist in the database' do
      it 'returns empty array' do
        expect(content).to be_empty
      end
    end

    context 'when scientists are in the database' do
      before { create_list(:scientist, scientist_count) }

      let(:scientist_count) { 3 }

      it 'returns an array with all scientists in the database' do
        expect(content.count).to eql scientist_count
      end
    end
  end
end
