# frozen_string_literal: true

RSpec.describe Insane::REST::Scientist, :destroy do
  describe 'DELETE /scientists/:id' do
    subject(:response) { delete "/scientists/#{id}" }

    context 'when scientist exists' do
      let(:scientist) { create(:scientist) }
      let(:id) { scientist.id }

      it { is_expected.to be_ok }

      it 'returns deleted scientist data' do
        expect(response.body).to eql scientist.values.to_json
      end
    end

    context 'when scientist does not exist' do
      let(:id) { create(:scientist).delete.id }

      it { is_expected.to be_not_found }
    end
  end
end
