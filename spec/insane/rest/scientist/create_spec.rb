# frozen_string_literal: true

RSpec.describe Insane::REST::Scientist, :create do
  describe 'POST /scientists' do
    subject(:response) { post '/scientists', params.to_json }

    context 'when data is valid' do
      let(:params) { attributes_for(:scientist) }

      it { is_expected.to be_created }

      it 'returns result of adding' do
        expect(response.body).not_to be_empty
      end

      describe 'result of adding' do
        subject(:result) { response.body }

        it 'matches the passed parameters' do
          expect(JSON.parse(result, symbolize_names: true)).to include(params)
        end
      end
    end

    context 'when data is not valid' do
      let(:params) { attributes_for(:scientist).except(:name) }

      it { is_expected.to be_bad_request }

      it 'returns errors' do
        expect(response.body).not_to be_empty
      end
    end
  end
end
