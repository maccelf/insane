# frozen_string_literal: true

RSpec.describe Insane::REST::Scientist, :update do
  describe 'PUT /scientists/:id' do
    subject(:response) { put "/scientists/#{id}", params.to_json }

    let(:id) { create(:scientist).id }
    let(:params) { attributes_for(:scientist) }

    context 'when scientist exists' do
      it { is_expected.to be_ok }

      it 'returns result of updating' do
        expect(response.body).not_to be_empty
      end

      describe 'result of updating' do
        subject(:result) { response.body }

        it 'matches the passed parameters' do
          expect(JSON.parse(result, symbolize_names: true)).to include(params)
        end
      end
    end

    context 'when scientist does not exist' do
      let(:id) { create(:scientist).delete.id }

      it { is_expected.to be_created }

      it 'returns result of adding' do
        expect(response.body).not_to be_empty
      end

      describe 'result of adding' do
        subject(:result) { response.body }

        it 'matches the passed parameters' do
          expect(JSON.parse(result, symbolize_names: true)).to include(params)
        end
      end
    end

    context 'when data is not valid' do
      let(:params) { attributes_for(:scientist, name: nil) }

      it { is_expected.to be_bad_request }

      it 'returns errors' do
        expect(response.body).not_to be_empty
      end
    end
  end

  describe 'PATCH /scientists/:id' do
    subject(:response) { patch "/scientists/#{id}", params.to_json }

    let(:id) { create(:scientist).id }
    let(:params) { attributes_for(:scientist) }

    context 'when scientist exists' do
      it { is_expected.to be_ok }

      it 'returns result of updating' do
        expect(response.body).not_to be_empty
      end

      describe 'result of updating' do
        subject(:result) { response.body }

        it 'matches the passed parameters' do
          expect(JSON.parse(result, symbolize_names: true)).to include(params)
        end
      end
    end

    context 'when scientist does not exist' do
      let(:id) { create(:scientist).delete.id }

      it { is_expected.to be_not_found }
    end

    context 'when data is not valid' do
      let(:params) { attributes_for(:scientist, name: nil) }

      it { is_expected.to be_bad_request }

      it 'returns errors' do
        expect(response.body).not_to be_empty
      end
    end
  end
end
