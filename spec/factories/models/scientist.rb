# frozen_string_literal: true

FactoryBot.define do
  factory :scientist, class: 'Insane::Models::Scientist' do
    to_create(&:save)

    sequence :name do |n|
      "Hubert J. Farnsworth#{n}"
    end

    insane { 54.516561 }
    destroy_galaxy { 5 }
  end
end
