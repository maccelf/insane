# frozen_string_literal: true

FactoryBot.define do
  factory :weapon, class: 'Insane::Models::Weapon' do
    to_create(&:save)

    sequence :name do |n|
      "Finger Extension#{n}"
    end

    power { 0.5661 }
    scientist_id { create(:scientist).id }
  end
end
