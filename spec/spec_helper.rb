# frozen_string_literal: true

require 'factory_bot'
require 'rack/test'
require_relative '../config/helper'

module RSpecMixin
  include Rack::Test::Methods
  def app
    Insane::REST::Controller
  end
end

RSpec.configure do |config|
  config.include RSpecMixin
  config.around do |example|
    LOG.debug(example.full_description)
    DB.transaction(rollback: :always, auto_savepoint: true) { example.run }
  end
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.find_definitions
  end
end
