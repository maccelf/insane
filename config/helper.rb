# frozen_string_literal: true

require_relative '../lib/insane'
require_relative 'initializers/logger'
require_relative 'initializers/database'
require_relative 'initializers/models'
require_relative 'initializers/rest'
