# frozen_string_literal: true

require_relative '../../lib/insane/models/scientist'
require_relative '../../lib/insane/models/weapon'
