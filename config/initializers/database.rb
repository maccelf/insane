# frozen_string_literal: true

require 'sequel'

DB = Sequel.postgres(
  host: 'localhost',
  user: 'vagrant',
  password: 'crazy_scientist',
  database: 'insane'
)

DB.logger = LOG
DB.sql_log_level = 'debug'
