# frozen_string_literal: true

require_relative '../../lib/insane/rest/helpers'
require_relative '../../lib/insane/rest/controller'
Dir[File.join(__dir__, '../../lib/insane/rest/*/', '*.rb')].sort.each do |file|
  require file
end

Insane::REST::Controller.configure do |settings|
  settings.set :bind, '0.0.0.0'
  settings.set :port, '8080'
  settings.set :server, :puma
  settings.enable :logging
end
