# frozen_string_literal: true

require 'logger'

format = '%Y-%m-%d %H:%M:%S'
procname = $PROGRAM_NAME
LOG = Logger.new(STDOUT)
LOG.progname = 'Insane'
LOG.level = ENV['LOG_LEVEL'] || 'debug'
LOG.formatter = proc do |severity, time, progname, msg|
  "[#{progname}: #{procname}] [#{time.strftime(format)}] #{severity}: #{msg}\n"
end
