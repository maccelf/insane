# frozen_string_literal: true

module Insane
  module Models
    # Класс модели Устройство
    class Weapon < Sequel::Model
      many_to_one :scientist

      # @!attribute name
      #   Название устройства
      #   @return [String]
      #     название устройства
      # @!attribute power
      #   Числовая характеристика разрушительной силы
      #   @return [Float]
      #     числовая характеристика разрушительной силы
      # @!attribute scientist_id
      #   Идентификатор учёного
      #   @return [Fixnum]
      #     идентификатор учёного
      # @!attribute datetime
      #   Дата и время добавления информации
      #   @return [DateTime]
      #     дата и время добавления информации
      # @!attribute [r] id
      #   Уникальный идентификатор устройства
      #   @return [Fixnum]
      #     уникальный идентификатор устройства
      # @!attribute scientist
      #   Учёный, которому принадлежит устройство
      #   @return [Scientist]
      #     учёный, которому принадлежит устройство

      # Добавляет дату создания устройства
      # @note Неявно вызывается при создании нового экземпляра класса
      def before_create
        super
        self.datetime = DateTime.now
      end

      plugin :validation_helpers
      # Проверяет корректность предоставляемых данных
      # @note Неявно вызывается при вызове метода valid?
      def validate
        super
        errors.add(:name, 'должно быть строкой') unless name.is_a?(String)
        validates_unique :name, message: 'должно быть уникальным'
        validate_power
        validate_scientist_id
      end

      private

      # Проверяет корректность атрибута `scientist_id`
      def validate_scientist_id
        if scientist_id.is_a?(Integer) && scientist_id.positive?
          unless Scientist.first(id: scientist_id)
            errors.add(:scientist_id,
                       'должно принадлежать существующему учёному')
          end
        else
          errors.add(:scientist_id, 'должно быть натуральным числом')
        end
      end

      # Проверяет корректность атрибута `power`
      def validate_power
        return if power.is_a?(Numeric) && power.positive?

        errors.add(:power, 'должно быть положительным вещественным числом')
      end
    end
  end
end
