# frozen_string_literal: true

module Insane
  # Модуль моделей
  module Models
    # Класс модели Учёный
    class Scientist < Sequel::Model
      one_to_many :weapons

      # @!attribute name
      #   Имя учёного
      #   @return [String]
      #     имя учёного
      # @!attribute insane
      #   Числовая характеристика безумности
      #   @return [Float]
      #     числовая характеристика безумности
      # @!attribute destroy_galaxy
      #   Количество попыток уничтожить Галактику
      #   @return [Fixnum]
      #     количество попыток уничтожить Галактику
      # @!attribute datetime
      #   Дата и время добавления информации
      #   @return [DateTime]
      #     дата и время добавления информации
      # @!attribute [r] id
      #   Уникальный идентификатор учёного
      #   @return [Fixnum]
      #     уникальный идентификатор учёного
      # @!attribute [r] weapons
      #   Зависимые устройства
      #   @return [Array<Weapon>]
      #     массив зависимых устройств

      # Добавляет дату создания учёного
      # @note Неявно вызывается при создании нового экземпляра класса
      def before_create
        super
        self.datetime = DateTime.now
      end

      plugin :validation_helpers
      # Проверяет корректность предоставляемых данных
      # @note Неявно вызывается при вызове метода valid?
      def validate
        super
        errors.add(:name, 'должно быть строкой') unless name.is_a?(String)
        validates_unique :name, message: 'должно быть уникальным'
        validate_insane
        validate_destroy_galaxy
      end

      private

      # Проверяет корректность атрибута `insane`
      def validate_insane
        return if insane.is_a?(Numeric) && insane.positive?

        errors.add(:insane, 'должно быть положительным вещественным числом')
      end

      # Проверяет корректность атрибута `destroy_galaxy`
      def validate_destroy_galaxy
        return if destroy_galaxy.is_a?(Integer) && !destroy_galaxy.negative?

        errors.add(:destroy_galaxy, 'должно быть натуральным числом')
      end
    end
  end
end
