# frozen_string_literal: true

module Insane
  # Константа версии приложения
  VERSION = '1.0.0'
end
