# frozen_string_literal: true

require_relative '../../version'

module Insane
  module REST
    # Модуль для показа версии приложения
    module Version
      Controller.get '/version' do
        Insane::VERSION
      end
    end
  end
end
