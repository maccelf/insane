# frozen_string_literal: true

require 'sinatra/base'

module Insane
  # Модуль REST API
  module REST
    # Класс контроллера приложения
    class Controller < Sinatra::Base
      helpers Helpers

      before do
        content_type 'application/json'
        if request.post? || request.put? || request.patch?
          request.body.rewind
          @body = get_parsed_response(request.body.read)
          halt 415 if @body.nil?
        end
      end
    end
  end
end
