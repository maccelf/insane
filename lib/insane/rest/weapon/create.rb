# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью устройств
    module Weapon
      # POST запрос для устройства
      Controller.post '/scientists/:id/weapons' do
        data = check_and_corrects_data_weapon(@body)
        data['scientist_id'] = params['id']
        weapon = Insane::Models::Weapon.new(data)
        halt [400, weapon.errors.to_json] unless weapon.valid?

        [201, weapon.save.values.to_json]
      end
    end
  end
end
