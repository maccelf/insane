# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью устройств
    module Weapon
      # GET запрос для всех устройств учёного
      Controller.get '/scientists/:id/weapons' do
        scientist = Insane::Models::Scientist[{ id: params[:id] }]
        halt 404 if scientist.nil?

        scientist.weapons.map(&:values).to_json
      end
    end
  end
end
