# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью устройств
    module Weapon
      # DELETE запрос для устройства
      Controller.delete '/weapons/:id' do
        weapon = Insane::Models::Weapon[{ id: params['id'] }]
        weapon.nil? ? 404 : [200, weapon.delete.values.to_json]
      end
    end
  end
end
