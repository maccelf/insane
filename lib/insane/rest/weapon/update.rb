# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью устройств
    module Weapon
      # PUT запрос для устройства
      Controller.put '/weapons/:id' do
        data = check_and_corrects_data_weapon(@body)
        weapon = Insane::Models::Weapon[{ id: params['id'] }]
        if weapon.nil?
          weapon = Insane::Models::Weapon.new(data)
          halt [400, weapon.errors.to_json] unless weapon.valid?

          [201, weapon.save.values.to_json]
        else
          weapon.set(data)
          halt [400, weapon.errors.to_json] unless weapon.valid?

          [200, weapon.save.values.to_json]
        end
      end

      # PATCH запрос для устройства
      Controller.patch '/weapons/:id' do
        data = check_and_corrects_data_weapon(@body)
        weapon = Insane::Models::Weapon[{ id: params['id'] }]
        halt 404 if weapon.nil?

        weapon.set(data)
        halt [400, weapon.errors.to_json] unless weapon.valid?

        [200, weapon.save.values.to_json]
      end
    end
  end
end
