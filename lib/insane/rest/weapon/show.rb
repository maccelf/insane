# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью устройств
    module Weapon
      # GET запрос для конкретного устройства
      Controller.get '/weapons/:id' do
        weapon = Insane::Models::Weapon[{ id: params['id'] }]
        weapon.nil? ? 404 : weapon.values.to_json
      end
    end
  end
end
