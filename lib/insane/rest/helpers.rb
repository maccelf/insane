# frozen_string_literal: true

module Insane
  # Модуль REST API
  module REST
    # Модуль вспомогательных методов для контроллера
    module Helpers
      # Корректные атрибуты учёного
      SCIENTIST_ATTRIBUTES = %w[name insane destroy_galaxy].freeze

      # Корректные атрибуты устройства
      WEAPON_ATTRIBUTES = %w[name power scientist_id].freeze

      # Проверяет и корректирует предоставляемые данные для учёного
      # @param data [Hash]
      #   данные учёного
      # @return [Hash]
      #   корректные данные учёного
      def check_and_corrects_data_scientist(data)
        return {} unless data.is_a?(Hash)

        data.delete_if { |key| !SCIENTIST_ATTRIBUTES.include?(key) }
      end

      # Проверяет и корректирует предоставляемые данные для устройства
      # @param [Hash] data
      #   данные устройства
      # @return [Hash]
      #   корректные данные устройства
      def check_and_corrects_data_weapon(data)
        return {} unless data.is_a?(Hash)

        data.delete_if { |key| !WEAPON_ATTRIBUTES.include?(key) }
      end

      # Получает данные из запроса
      # @param [JSON] body
      #   тело запроса в формате JSON
      # @return [Hash]
      #   данные, если JSON валидный
      # @return [nil]
      #   nil, если JSON не валидный
      def get_parsed_response(body)
        JSON.parse(body)
      rescue JSON::ParserError
        nil
      end
    end
  end
end
