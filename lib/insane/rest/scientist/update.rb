# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью учёный
    module Scientist
      # PUT запрос для учёного
      Controller.put '/scientists/:id' do
        data = check_and_corrects_data_scientist(@body)
        scientist = Insane::Models::Scientist[{ id: params['id'] }]
        if scientist.nil?
          scientist = Insane::Models::Scientist.new(data)
          halt [400, scientist.errors.to_json] unless scientist.valid?

          [201, scientist.save.values.to_json]
        else
          scientist.set(data)
          halt [400, scientist.errors.to_json] unless scientist.valid?

          [200, scientist.save.values.to_json]
        end
      end

      # PATCH запрос для учёного
      Controller.patch '/scientists/:id' do
        data = check_and_corrects_data_scientist(@body)
        scientist = Insane::Models::Scientist[{ id: params['id'] }]
        halt 404 if scientist.nil?

        scientist.set(data)
        halt [400, scientist.errors.to_json] unless scientist.valid?

        [200, scientist.save.values.to_json]
      end
    end
  end
end
