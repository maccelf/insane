# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью учёный
    module Scientist
      # GET запрос для конкретного учёного
      Controller.get '/scientists/:id' do
        scientist = Insane::Models::Scientist[{ id: params['id'] }]
        scientist.nil? ? 404 : scientist.values.to_json
      end
    end
  end
end
