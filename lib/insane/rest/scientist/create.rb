# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью учёный
    module Scientist
      # POST запрос для учёного
      Controller.post '/scientists' do
        data = check_and_corrects_data_scientist(@body)
        scientist = Insane::Models::Scientist.new(data)
        halt [400, scientist.errors.to_json] unless scientist.valid?

        [201, scientist.save.values.to_json]
      end
    end
  end
end
