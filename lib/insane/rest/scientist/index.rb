# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью учёный
    module Scientist
      # GET запрос для всех учёных
      Controller.get '/scientists' do
        Insane::Models::Scientist.all.map(&:values).to_json
      end
    end
  end
end
