# frozen_string_literal: true

module Insane
  module REST
    # Модуль для взаимодействия с моделью учёный
    module Scientist
      # DELETE запрос для учёного
      Controller.delete '/scientists/:id' do
        scientist = Insane::Models::Scientist[{ id: params['id'] }]
        scientist.nil? ? 404 : [200, scientist.delete.values.to_json]
      end
    end
  end
end
